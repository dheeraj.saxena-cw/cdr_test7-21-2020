# `OTSUKA CHARGEBACK` Data Pipeline
This project contains the Scala code and configurations to support the `chargeback` data pipeline. 

## **Data Sources**
The table below details the `chargeback` data sources and their corresponding target tables:

| Description | Value |
| :------ | :-------: |
| File Delimiter    | `|` |
| File Extension    | `".txt"` |
| File Type         | `TEXT` |
| Quoted Text       | `true` |
| Date Format       | `N/A` |
| Timestamp Format  | `MM/dd/yyyy HH:mm:ss` | 


| Data Source Description | S3 Source Folder  | S3 Target Folder | Redshift Target Table(optional) | Replacement Policy |
| :------ | :------ | :------ | :------ | :------- |
| cardinal_chargeback_mnthly | raw/cardinal_chargeback_mnthly | chargeback/allchargebacks | chargeback.allchargebacks | Append |


## **S3 Bucket Structure**
This section describes the project structure within S3.<br>
Note: The folder structure between development and production environments are the same except for the name of the root S3 bucket

| Directory | S3 Path/Location | Description |
| :------ | :------ | :------ |
| Root S3 Bucket            | DEV: `s3://otsuka-us-chargeback-dev`<br>PROD: `s3://otsuka-us-chargeback-prod` | Project Folder
| Raw files                 | `<root_s3_bucket>/raw`                    | Stores all the individual sources |
| Individual data source    | `<root_s3_bucket>/raw/<data_source>`      | Stores raw files per data source |
| Target                    | `<root_s3_bucket>/<target_db>`            | Stores processed data in ORC format |
| Scripts                   | `<root_s3_bucket>/scripts`                | Stores the project specific Glue script(s) |
| Schemas                   | `<root_s3_bucket>/schemas`                | Stores raw file schemas (in JSON format) |
| Library                   | `<root_s3_bucket>/lib`                    | Stores dependent jar files; as defined by the `--extra-jars` |
| Configuration             | `<root_s3_bucket>/config/core`            | Stores Core Pipeline Configuration file(s) |
| Configuration             | `<root_s3_bucket>/config/source`          | Stores Indivdual Source Configuration file(s) |
| Configuration             | `<root_s3_bucket>/config/transform`       | Stores Indivdual Source Transformation Configuration file(s) |

## **Pipeline Configuration**
The `chargeback` pipeline is configured as follows:

### AWS Environment Variables
| Variable | Value | Note |
| :------ | :------: | :------ |
| REGION                | `default` | Set at project level |
| CLOUDWATCH_URL        | `default` | Set at project level |
| CLOUDWATCH_ERROR_URL  | `default` | Set at project level |
| AWS_ACCESS_KEY_ID     | `default` | Set at project level |
| AWS_SECRET_ACCESS_KEY | `default` | Set at project level |
| AWS_SESSION_TOKEN     | `default` | Set at project level |

### Control and Version Variables
| Variable | Value | Notes |
| :--------- | :------: | :---------|
| DATA_PIPELINE_COMMON_VER  | `2.2.3-RELEASE`   | Framework version used |
| Template Stages Version   | `1.5.3`           | ... |
| STAGE_S3_FILES            | `default`         | Set at project level | 
| BUILD_CONTAINER           | `default`         | Set at project level |
| AWS_GLUE_VERSION          | `1.0`             | AWS glue version |

### Project Variables
| Variable | Value | Notes |
|:---------|:------:|:---------|
| S3_BUCKET_ROOT_DEV        | `s3://otsuka-us-chargeback-dev`   | ... |
| S3_BUCKET_ROOT_PROD       | `s3://otsuka-us-chargeback-prod`  | ... |
| AWS_DEFAULT_REGION_DEV   	| `us-east-2`  						| ... |
| AWS_DEFAULT_REGION_PROD  	| `us-east-1`  						| ... |

#### AWS Glue ETL Variables
| Variable | Value | Notes |
|:------|:------:|:------|
| JOB_NAME_DEV              | `OTSUKA CHARGEBACK: CDR_STG Pipeline(Dev)`    | ... |
| JOB_NAME_PROD             | `OTSUKA CHARGEBACK: CDR_STG Pipeline(Prod)`   | ... |
| GLUE_ROLE                 | `default`                                     | Set at project level |
| PIPELINE_CFG_FILE_DEV     | `chargeback-core-cfg-dev.yaml`                | ... |
| PIPELINE_CFG_FILE_PROD    | `chargeback-core-cfg-prod.yaml`               | ... |
| MAX_CAPACITY_DEV          | `2`                                           | ... |
| MAX_CAPACITY_PROD         | `4`                                           | ... |
| GLUE_SCRIPT_DEV           | `chargeback-glue-job.scala`                   | ... |
| GLUE_SCRIPT_PROD          | `chargeback-glue-job.scala`                   | ... |
| SOURCE_INPUT_URI_DEV      | `s3://otsuka-us-chargeback-dev`               | ... |
| SOURCE_INPUT_URI_PROD     | `s3://otsuka-us-chargeback-prod`              | ... |
| DATA_SINK_URI_DEV         | `s3://otsuka-us-chargeback-dev`               | ... |
| DATA_SINK_URI_PROD        | `s3://otsuka-us-chargeback-prod`              | ... |

#### Catalog (crawler) Variables
| Variable | Value | Notes |
|:------|:------:|:------|
| CWLR_NAME_DEV             | `OTSUKA CHARGEBACK: CDR_STG Crawler (Dev)`                    | ... |
| CWLR_NAME_PROD            | `OTSUKA CHARGEBACK: CDR_STG Crawler (Prod)`                   | ... |
| CWLR_DBNAME_DEV           | `chargeback`                                                  | ... |
| CWLR_DBNAME_PROD          | `chargeback`                                                  | ... |
| CWLR_DESC_DEV             | `Catalogs chargeback CDR_STG layer`                           | ... |
| CWLR_DESC_PROD            | `Catalogs chargeback CDR_STG layer`                           | ... |
| CWLR_TGTS_DEV             | `s3://otsuka-us-chargeback-dev/chargeback/allchargebacks`     | ... |
| CWLR_TGTS_PROD            | `s3://otsuka-us-chargeback-prod/chargeback/allchargebacks`    | ... |
| CWLR_SCHEMA_POLICY_DEV    | `UPDATE_IN_DATABASE`<br>`DeleteBehavior=DELETE_FROM_DATABASE` | ... |
| CWLR_SCHEMA_POLICY_PROD   | `UPDATE_IN_DATABASE`<br>`DeleteBehavior=DELETE_FROM_DATABASE` | ... |

#### Redshift Database Variables
| Variable | Value | Notes |
|:------|:------:|:------|
| PGHOST                    | `default`          | Set at project level |
| PGPASSWORD                | `default`          | Set at project level |
| PGPORT                    | `default`          | Set at project level |
| PGUSER                    | `default`          | Set at project level |
| PGDATABASE_DEV            | `cdr_dev`          | ... |
| PGDATABASE_PROD           | `cdr`              | ... |
| EXTERNAL_SCHEMA_DEV       | `chargeback`       | ... |
| EXTERNAL_SCHEMA_PROD      | `chargeback`       | ... |
| EXTERNAL_DATABASE_DEV     | `chargeback`       | ... |
| EXTERNAL_DATABASE_PROD    | `chargeback`       | ... |