/*
 * Copyright (c) 2019 ODH Solutions, Inc.
 * All Rights Reserved. Proprietary and confidential information of ODH.
 * Disclosure, use, or reproduction without the written authorization of ODH is prohibited.
 */

import com.amazonaws.services.glue.ChoiceOption
import com.amazonaws.services.glue.GlueContext
import com.amazonaws.services.glue.MappingSpec
import com.amazonaws.services.glue.ResolveSpec
import com.amazonaws.services.glue.errors.CallSite
import com.amazonaws.services.glue.util.GlueArgParser
import com.amazonaws.services.glue.util.Job
import com.amazonaws.services.glue.util.JsonOptions
import com.odh.pipeline.core.config.CoreConfiguration
import com.odh.pipeline.core.qc.DeclarativeVerificationResult
import com.odh.pipeline.core.schema.StructTypeFactory
import com.odh.pipeline.dataframe.{DataFrameBuilder, DataSinkWriter}
import com.odh.pipeline.util.log.FrameworkLogging
import org.apache.spark.internal.Logging
import org.apache.spark.SparkContext
import org.apache.spark.sql.SaveMode
import java.net.URI

import com.odh.pipeline.transform.TransformRunner
import com.odh.pipeline.util.log.LogType

import scala.collection.JavaConverters._

/**
 * Created by Dennis Wendell (dennis17480) on 9/24/19.
 */
object GlueApp extends FrameworkLogging {
  def main(sysArgs: Array[String]) {
    val sc: SparkContext = new SparkContext()
    val glueContext: GlueContext = new GlueContext(sc)
    val spark = glueContext.getSparkSession
    val args =
      GlueArgParser.getResolvedOptions(
        sysArgs,
        Seq("JOB_NAME", "SOURCE_INPUT_URI", "PIPELINE_CFG", "DATA_SINK_URI","ENVIRONMENT").toArray
      )

    spark.conf.set("SOURCE_INPUT_URI", args("SOURCE_INPUT_URI"))
    spark.conf.set("DATA_SINK_URI", args("DATA_SINK_URI"))
	  spark.conf.set("ENVIRONMENT", args("ENVIRONMENT"))
	  spark.conf.set("JOB_NAME", args("JOB_NAME"))
    spark.conf.set("BATCH_ID_ENABLED", "false") 
    Job.init(args("JOB_NAME"), glueContext, args.asJava)

    val uri = args("PIPELINE_CFG")

    /** used to accumulate all results for all data sources, evaluated at the end of processing */
    var verificaionResultsForAllSources = Map[String, DeclarativeVerificationResult]()

    CoreConfiguration.init(new URI(uri))
    CoreConfiguration.dataSources.foreach { sourceConfig =>
      log.info(f"Processing data source '${sourceConfig.sourceName}'")

      // Read CSV file into dataframe
      val csvDF = new DataFrameBuilder().fromSourceConfiguration(sourceConfig, unTyped = true)

      csvDF.printSchema()
      csvDF.show()

      // Perform quality checks on 'raw' data
      log.info("Performing quality checks on raw dataframe...")
      val verificationResults =
        new DeclarativeVerificationResult(
          sourceConfig,
          CoreConfiguration.config.verificationRepositoryUri.toString,
          csvDF
        )

      println("all results:")
      verificationResults.all().show()
      fwLogInfo(this, LogType.DataQuality, f"Total number of quality checks performed for '${sourceConfig.sourceName}': ${verificationResults.all().count()}")

      /** append current verification results to the map of all data source verification results, keyed by source name */
      verificaionResultsForAllSources += (sourceConfig.sourceName -> verificationResults)

      fwLogInfo(this, LogType.DataQuality, f"Total number of failed quality checks performed for '${sourceConfig.sourceName}': ${verificationResults.failed().count()}")
      verificationResults.failed().show()

      fwLogInfo(this, LogType.DataQuality, f"Total number of successful quality checks performed for '${sourceConfig.sourceName}': ${verificationResults.successful().count()}")
      verificationResults.successful().show()

      println("metrics as dataframe:")
      val metricsAsDataFrame = verificationResults.successMetricsAsDataFrame()
      metricsAsDataFrame.show

      // checks as dataframe
      println("checks as dataframe:")
      val checksAsDataFrame = verificationResults.checkResultsAsDataFrame()
      checksAsDataFrame.show

      println("results as json:")
      val metricsAsJson = verificationResults.successMetricsAsJson()
      metricsAsJson.collect().foreach(println)

      // Free initial dataframe, marking for deletion from memory
      csvDF.unpersist()

      /** if errors in QC, skip further processing of this data source, will terminate pipeline once all data sources have been processed */
      if (verificaionResultsForAllSources.valuesIterator.exists(_.error().count() > 0)) {
        fwLogWarn(this, LogType.Core, f"Skipping further processing of data source '${sourceConfig.sourceName}' due to earlier QC failure(s)")
      } else { // continue processing data source
        // Re-read initial CSV, applying the configured schema to the data
        val sourceDF = new DataFrameBuilder().fromSourceConfiguration(sourceConfig)

        if (sourceConfig.sourceDetails.transformConfigurationUri.isDefined) {
          // Perform transformation(s)
          val transformedDF = TransformRunner().onData(sourceDF)
            .fromConfigUri(sourceConfig.sourceDetails.transformConfigurationUri.orNull)
            .run()
          new DataSinkWriter(transformedDF).forSource(sourceConfig)
        }
        else {
          new DataSinkWriter(sourceDF).forSource(sourceConfig)
        }

      }
    }
    Job.commit()
  }
}
