# 🐞Bug Report 🐛

## **Current Behavior**

[Enter short description of current behavior]

```js

<Add any code here>

```

## **Expected Behavior**
[Enter a clear and concise description of what is expected (or code)]

```js

<Add any code here>

```

## **Possible Solution**
[Enter a suggestion or snippit of code you would like to share]


## **Steps to Reproduce**
[Add any other context about the problem here]

1. [Item 1]
2. [Item 2]
   * [Unordered sub-list]
3. [Item 3]