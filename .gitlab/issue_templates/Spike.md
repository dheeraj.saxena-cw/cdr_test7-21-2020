# 🏈 Description 🕵️‍
[Detailed description of research, design, exploration or investigation]

[Simple list Template] 
1. [Item 1]
2. [Item 2]
   * [Unordered sub-list]
3. [Item 3]
   1. [Ordered sub-list]
4. [Item 4]


# 🔎 Acceptance Criteria 🔍

[Add deliverable method or result here or use pre-defined options below] 
- [x] Document in Confluence
- [x] Document in Google Drive (Location: `URL`)
- [x] Document in Task
- [x] Estimate for corresponding Story
- [x] Discussion with team
- [x] Other:
