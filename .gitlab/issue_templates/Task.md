# :pushpin: Description :pushpin:
[Detailed description of the task or feature]

[Simple list Template] 
1. [Item 1]
2. [Item 2]
   * [Unordered sub-list]
3. [Item 3]
   1. [Ordered sub-list]
4. [Item 4]

[Check list Template] 
- [x] Sub Task
- [ ] Sub Task
    - [ ] Sub-task 1
    - [x] Sub-task 2
    - [ ] Sub-task 3

# :white_check_mark: Acceptance Criteria :white_check_mark:
[Expected behavior after completion of the ticket]